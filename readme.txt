Nachdem das System gestartet beziehungsweise das Debian-Paket installiert ist, kann ein Browser geöffnet werden und http://guidesystem.localhost eingegeben werden. Die Lerneinheiten vom Trainingssystem können anschließend durchlaufen werden.

*Start mit Vagrant*
Download der letzten Version unter http://files.timo-pagel.de/php-security-trainig-system/php-security-training-system-vagrant.tar.
Entpacken und wechseln in den Ordner vagrant. Ausführen des Kommandos:
vagrant plugin install vagrant-hostsupdater
vagrant up

*Start mit git-checkout (Ubuntu)*
cd /tmp/ # any other folder is allowed, too
git clone git@bitbucket.org:tpagel/php-security-training-system.git
sudo ln -s /tmp/php-security-training-system/ /var/www/php-security-training-system
sudo ln -s /tmp/php-security-training-system//LiveDVD/debian/etc/apache2/sites-available/guidesystem.localhost.conf /etc/apache2/sites-enabled
echo "127.0.0.1 guidesystem.local" >> /etc/hosts
echo "127.0.0.1 unitsystem.local" >> /etc/hosts
a2enmod vhost_alias
cd  /tmp/php-security-training-system/guidesystem.localhost
./composer.phar update



*Start mit LiveDVD*
Nach Start der LiveDVD kann Deutsch als Sprache ausgewählt werden und das System im Modus "Ausprobieren" gestartet werden. 

Um das System in den Ausliferungszustand zu bringen, kann die Datei scripts/setup.bash ausgeführt werden.
Um lediglich den Lerneinheiten-Status (abgeschlossene/aktive Aufgabe(n)) zurück zu setzen, kann das Script scripts/chmodFolders.bash ausgeführt werden. 

*Anleitung für einen Übungsleiter*
-Anlegen einer Lerneinheit:
Lerneinheiten können kopiert und angepasst werden.

-Änderung der Reihenfolge von Lerneinheiten:
Innerhalb der Datei unitDefinition.xml können Lerneinheiten durch angabe der Ebene im Tag level sortiert werden.

-Änderung der Reihenfolge von Kategorien
Der Ordnername einer Kategorie kann geändert werden. Wird beispielsweise eine 0 vorran gestellt, so erscheint diese als erstes. Dabei ist zu beachten, dass in der Datei categoryDefiniton das Tag directory ebenfalls angepasst wird.