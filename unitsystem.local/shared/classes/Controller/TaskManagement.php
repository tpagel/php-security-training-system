<?php
namespace UnitSystem\Controller;
use \GuideSystem\Exception\LogException;

class FileNotFoundException extends LogException {
}

class TaskManagement {
	CONST PROCESS_FILE_NAME = "processFile.php";
	private $filepath;
	private $name;
	private $activeTask;

	public function __construct(\GuideSystem\Model\Unit $unit) {
		$this->name = $unit->getName();
		$this->filepath = realpath(dirname(__FILE__)) . "/";
		$this->activeTask = $unit->getActivePart()->getActiveTask();
		if(!($this->activeTask instanceof \GuideSystem\Model\Task\Attack\ApplicationText)) {
			throw new LogException("Task is not an instance of Attack");
		}
	}

	public function processRequest() {
		$this->displayHead();
		$actualDir = getcwd();
		chdir($this->filepath);
		include($this->filepath . self::PROCESS_FILE_NAME);
		chdir($actualDir);
		$this->displayFoot();
	}
	/**
	 *
	 * @param String $filepath            
	 * @throws Exception
	 */

	public function setFilepath($filepath) {
		$filepath = realpath($filepath) . "/";
		if ($filepath === false) {
			throw new FileNotFoundException("File not found");
		}
		$this->filepath = $filepath;
	}

	private function displayHead() {
		echo "<html><head>
				<title>" . $this->name
				. "</title>
				<meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">
				<script src=\"plugins/jquery-1.9.1.js\"></script>
				<script src=\"plugins/jquery-ui-1.10.3.custom.js\"></script>
				<script src=\"js/tooltip.js\"></script>
				<link rel=\"stylesheet\" href=\"css/ui-lightness/jquery-ui-1.10.3.custom.min.css\" />	
			</head><body>";
	}

	private function displayFoot() {
		echo "</body></html>";
	}
}
