<?php
namespace UnitSystem\Controller;
class RequestManager extends \GuideSystem\Controller\RequestManagerBase {
	private $activeUnit;
	private $activePartPath;
	public function __construct() {
		$this->loadCategoryManager();
		$this->detectAndSetActiveUnit();
		$this->processRequest();
	}
	private function detectAndSetActiveUnit() {
		$activeCategory = $this->categoryManager->getActiveCategory();
		if ($activeCategory == null) {
			die("Es wurde keine Lehreinheit ausgewaehlt");
		}
		$this->activeUnit = $activeCategory->getActiveUnit();
		$activePart = $this->activeUnit->getActivePart();
		$this->activePartPath = $activePart->getDirectoryPath();
	}
	private function processRequest() {
		if (isset($_GET['image'])) {
			$this->processObject("image");
		}else if (isset($_GET['object'])) {
			$this->processObject("object");
		}else {
			try {
				$taskManagement = new TaskManagement($this->activeUnit);
				$taskManagement->setFilepath($this->activePartPath);
				$taskManagement->processRequest();
			} catch(\Exception $e) {
				die("Fehler: {$e->getMessage()}");
			}
		}
	}
	private function processObject($objectType) {
		$fileDirectoryPath = realpath($this->activePartPath . "/" . $objectType . "s/") . "/";
		$object = $this->getObject($fileDirectoryPath, $_GET[$objectType]);
		$contentType = mime_content_type(
				$fileDirectoryPath . "/" . $_GET[$objectType]);
		header("Content-type: $contentType");
		echo $object;
		exit();
	}

	private function getObject($directoryPath, $name) {
		$filepath = realpath($directoryPath . "/" . $name . "");
		if (!preg_match("#" . $directoryPath . "#", $filepath)) {
			die("Unerwartete Eingabe!");
		}
		$object = file_get_contents($filepath);
		return $object;
	}
}
