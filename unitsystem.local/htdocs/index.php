<?php
require_once ("../../guidesystem.local/shared/configuration/config.php");
require_once (GUIDE_HTDOCS_DIR . "/../vendor/autoload.php");
require_once (GUIDE_HTDOCS_DIR . "/../shared/configuration/initialize.php");

try{
	$controller = new UnitSystem\Controller\RequestManager();
}catch(\Exception $e) {
	$logDir = realpath(getcwd() . "/../logs/");
	echo "Es ist ein unerwarteter Fehler aufgetreten, bitte reichen Sie das Fehler-Protokoll aus dem Ordner <em>{$logDir}/trainingsystem.log</em> ein.";
}