<h1>Anmeldung</h1>
<?php
$isUserValid = false;
$validUser = file_get_contents("user.txt");
$passwordHash = '$2y$12$c2VjdXJlU2FsdHNTaG91b.NnkRM1xxMm6Ml0vt7BrEHtoODukVNSe';
$isPasswordValid = password_verify($_POST['password'], $passwordHash);
if (isset($_POST['username']) && isset($_POST['password']) && $_POST['username'] == $validUser) {
    if ($isPasswordValid) {
        $isUserValid = true;
    } else {
        $isUserValid = false;
    }
}
if ($isUserValid) {
    echo "<strong style='color: green;'>Anmeldung erfolgreich</strong>";
    return;
} else 
    if (isset($_POST['username'])) {
        echo "<strong style='color: red;'>Anmeldung nicht erfolgreich</strong>";
    }

?>
<form method="POST">
	<label>Benutzername</label><input type="text" name="username"
		class="tooltip"
		title="Messe die Ausführungszeit bei Angabe von verschiedenen Benutzern. Administrative Benutzer heißen oft: admin, administrator, superuser, root oder superadmin." /><br />
	<label>Passwort</label><input type="password" name="password" /><br />
	<input type="submit" />
</form>