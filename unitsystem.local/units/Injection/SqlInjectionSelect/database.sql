DROP TABLE IF EXISTS wine;

CREATE TABLE wine (
	variety VARCHAR(40),
	article VARCHAR(40)
);

INSERT INTO wine (variety, article)
	VALUES('lagrein', 'Leckerer südtiroler Wein'); 
INSERT INTO wine (variety, article)
	VALUES('lagrein', 'Leckerer tiroler Wein'); 
INSERT INTO wine (variety, article)
	VALUES('lagrein', 'Super leckerer Südtiroler'); 
INSERT INTO wine (variety, article)
	VALUES('white', 'Weißer Wein'); 
INSERT INTO wine (variety, article)
	VALUES('white', 'Weißer Wein'); 
INSERT INTO wine (variety, article)
	VALUES('uncategorized', 'Spezial-Wein für die VIP-Besucher'); 
INSERT INTO wine (variety, article)
	VALUES('uncategorized', 'Spezial-Wein für die VIP-Besucher1'); 
INSERT INTO wine (variety, article)
	VALUES('uncategorized', 'Spezial-Wein für die VIP-Besucher2'); 
INSERT INTO wine (variety, article)
	VALUES('uncategorized', 'Spezial-Wein für die VIP-Besucher4'); 
INSERT INTO wine (variety, article)
	VALUES('uncategorized', 'Spezial-Wein für die VIP-Besucher5'); 
INSERT INTO wine (variety, article)
	VALUES('uncategorized', 'Spezial-Wein für die VIP-Besucher6'); 
INSERT INTO wine (variety, article)
	VALUES('uncategorized', 'Château1');
INSERT INTO wine (variety, article)
	VALUES('uncategorized', 'Château2');	
	
