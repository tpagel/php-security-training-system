<h1
	style="background-image: url(?image=frenchwinebottle.png); background-repeat: no-repeat; padding-left: 20px;">Weinkatalog</h1>
<h2>Bitte geben Sie die gewünschte Sorte an:</h2>
<form method="POST">
	<select name="variety" size="3">
		<option value="white">Weißwein</option>
		<option value="lagrein">Rebsorten</option>
	</select> <input type="submit" />
</form>

<?php
if (isset($_REQUEST['variety'])) {
    $databaseHandle = new PDO('sqlite:../database.db');    
    $databaseHandle->exec("set names utf8");
    $variety = $_REQUEST['variety'];
    $query = "SELECT * FROM wine WHERE variety=:variety";
    $stmt = $databaseHandle->prepare($query);
    $stmt->bindParam(":variety", $variety);
    $stmt->execute();
    $i = 1;
    echo "<table border=1>";
    echo "<tr><td>Nummer</td><td>Sorte</td><td>Artikel</td></tr>";
    while ($row = $stmt->fetch()) {
        echo "<tr>";
        echo "<td>" . $i ++ . "</td>";
        echo "<td>" . htmlspecialchars($row['variety']) . "</td>";
        echo "<td>" . htmlspecialchars($row['article']) . "</td>";
        echo "</tr>";
    }
    echo "</table>";
}
?>
