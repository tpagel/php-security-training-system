SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Member`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`Member` (
  `login` VARCHAR(45) NOT NULL ,
  `email` VARCHAR(255) NULL ,
  `password` CHAR(32) NULL ,
  `passwordEncryption` VARCHAR(45) NULL ,
  PRIMARY KEY (`login`) )
ENGINE = InnoDB;

USE `mydb` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `mydb`.`Member`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`Member` (`login`, `email`, `password`, `passwordEncryption`) VALUES ('tom', 'tom@example.com', '4ba766482eac20519e7086b1b26d1a64', 'md5');
INSERT INTO `mydb`.`Member` (`login`, `email`, `password`, `passwordEncryption`) VALUES ('till', 'till@example.com', 'c895f10abe6b0456b5ca1b949d3d084e', 'md5');
INSERT INTO `mydb`.`Member` (`login`, `email`, `password`, `passwordEncryption`) VALUES ('bob', 'bob@example.com', 'de9e9fd4f72e1621d8f80f7ad09a79ba', 'md5');

COMMIT;
