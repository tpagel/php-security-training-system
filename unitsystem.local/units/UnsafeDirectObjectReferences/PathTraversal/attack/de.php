<?php
$translation = array();
$translation['welcome'] = "Herzlich Willkommen!";
$translation['subtitle'] = "Hier bekommst du für 50 Euro einen Tipp, welche Aktie morgen steigen wird!";
$translation['blz'] = "Bankleitzahl";
$translation['number'] = "Nummer";
$translation['name'] = "Name";
$translation['data'] = "Daten";
$translation['email'] = "Email";
$translation['thankyou'] = "Vielen Dank für Ihre Eingabe, wir werden die 50 Euro per Lastschrift von Ihrem Konto abziehen und Ihnen anschlißenend einen Link zusenden";