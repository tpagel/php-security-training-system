<?php
if (!array_key_exists("language", $_GET)) {
	redirect("?language=de");
}

$allowedPublicDirectoryPath = dirname(__FILE__); // This directory
$requestedFilePath = realpath($allowedPublicDirectoryPath . "/" . $_GET['language'] . ".php");
if ($requestedFilePath === false) {
	throw new Exception("Systemfehler, angeforderte Sprache ist nicht im System vorhandenen");
} else if (!preg_match("#^" . $allowedPublicDirectoryPath . "#", $requestedFilePath)) {
	throw new Exception("Zugriff verweigert");
} else {
	require_once($requestedFilePath);
}
if (!isset($translation)) { // Old: die
	throw new Exception("Systemfehler, Sprachen wurden nicht initialisiert");
}

if (!empty($_POST)) {
	echo $translation['thankyou'];
} else {
	echo '<img src="?image=stockup.png" width="40px" height="30px"/><h1>' . $translation['welcome'] . "</h1>";
	echo "<h2>" . $translation['subtitle'] . "</h2>";
	echo "<h3>" . $translation['data'] . ":</h3>";
	echo "<form method=\"POST\">";
	echo "<label>" . $translation['blz'] . '</label><input type="text" name="blz" /><br />';
	echo "<label>" . $translation['number'] . '</label><input type="text" name="number" /><br />';
	echo "<label>" . $translation['name'] . '</label><input type="text" name="name" /><br />';
	echo "<label>" . $translation['email'] . '</label><input type="text" name="email" /><br />';
	echo '<input type="submit" />';
	echo "</form>";
}
