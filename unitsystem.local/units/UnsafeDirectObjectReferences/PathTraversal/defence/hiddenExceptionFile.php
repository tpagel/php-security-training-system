<?php
namespace {
class HiddenFileException extends \Exception {
}
throw new HiddenFileException("This should not be possible");
}
