#!/bin/bash
# This script updates composer libraries

SCRIPT_FILE=$(readlink -f $0)
SCRIPT_PATH=$(dirname $SCRIPT_FILE)
GUIDE_SYSTEM_PATH=$SCRIPT_PATH/../guidesystem.local
COMPOSER_VENDOR_PATH=$GUIDE_SYSTEM_PATH/vendor/

cd $GUIDE_SYSTEM_PATH
php composer.phar self-update
php composer.phar install