#!/bin/bash

function deleteFolderFiles {
	for file in $(find ../ -name .delete);
	do 
		FOLDER_PATH=$(dirname $file)
		if [ $(ls $FOLDER_PATH | wc -l) -gt 0 ]; then
			rm -R $FOLDER_PATH/*;
		fi
	done
}

deleteFolderFiles