#!/bin/bash

function chmodFolders {
	for file in $(find ../ -name .chmod);
	do
		RIGHTS=$(cat $file)
		FOLDER_PATH=$(dirname $file)	
		chmod -R $RIGHTS $FOLDER_PATH
	done
}

chmodFolders
