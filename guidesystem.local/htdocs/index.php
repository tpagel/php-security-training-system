<?php
define('HTDOCS_DIR', realpath(dirname(__FILE__)));
require_once ("../shared/configuration/config.php");
require_once (GUIDE_HTDOCS_DIR . "/../vendor/autoload.php");
require_once (GUIDE_SHARED_DIR . "/configuration/initialize.php");
require_once (GUIDE_SHARED_DIR . "/configuration/smarty.php");

try {
    $controller = new \GuideSystem\Controller\RequestManager($smarty);
} catch (Exception $e) {
	$logDir = realpath(getcwd() . "/../logs/");
	echo "Es ist ein unerwarteter Fehler aufgetreten, bitte reichen Sie das Fehler-Protokoll aus dem Ordner <em>{$logDir}/trainingsystem.log</em> ein.";
}
