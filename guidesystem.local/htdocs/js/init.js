/*
var myCodeMirror = CodeMirror(document.body, {
	value : "function myScript(){return 100;}\n",
	mode : "PHP"
})
 */;
 var myCodeMirror, myCodeMirror2; // They are global defined to detected that codemirror for a specific class has been initiated already (otherwise there would be multiple code-boxes without viewable content)
$(document).ready(
		function() {
			/*
			$('#sourceCodeButton').click(
					function() {

						$('.sourceCodeDiv').toggle();

						if (typeof myCodeMirror == "undefined") {
							myCodeMirror = CodeMirror.fromTextArea($(
									'.editSourceCode').get(0), {
								lineNumbers : true,
								matchBrackets : true,
								mode : "application/x-httpd-php",
								indentUnit : 4,
								indentWithTabs : true,
								enterMode : "keep",
								tabMode : "shift",
								readOnly : true
							});
						}
					});
*/
			$('#helperSourceCodeButton').click(
					function() {
						$('.helperSourceCodeDiv').toggle();
						if (typeof myCodeMirror2 == "undefined") {
							myCodeMirror2 = CodeMirror.fromTextArea($(
									'#helperSourceCode').get(0), {
								lineNumbers : true,
								matchBrackets : true,
								mode : "application/x-httpd-php",
								indentUnit : 4,
								indentWithTabs : true,
								enterMode : "keep",
								tabMode : "shift",
								readOnly : true
							});
						}
					});

			$('#helperSolutionButton').click(function() {
				$('.solutionDiv').toggle();
			});
		});
$.toast.config.closeForStickyOnly = false;