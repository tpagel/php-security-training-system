<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de-DE" lang="de-DE" dir="ltr">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="author" content="Timo Pagel" />
		<title>PHP-Sicherheits-Traingsystem</title>

		<link rel="stylesheet" href="css/lyx.css" type="text/css" />
		<link href="/templates/protostar/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
		<link rel="stylesheet" href="css/template.css" type="text/css" />
	  
		<script src="plugins/jquery-1.9.1.js"></script>
		<script src="plugins/jquery.toast.min.js"></script>
		<script src="plugins/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.js"></script>
		
		<script src="plugins/codemirror/lib/codemirror.js"></script>
		<script src="plugins/codemirror/mode/php/php.js"></script>
		<script src="plugins/codemirror/mode/xml/xml.js"></script>
		<script src="plugins/codemirror/mode/clike/clike.js"></script>
		
		<script src="js/init.js"></script>
		
		<link rel="stylesheet" href="plugins/codemirror/lib/codemirror.css">
		<link rel="stylesheet" href="css/jquery.toast.min.css">
		<link rel="stylesheet" href="css/guidesystem.css">
				<link rel="stylesheet" href="plugins/jquery-ui-1.10.3.custom/css/ui-lightness/jquery-ui-1.10.3.custom.css">
		{*<link rel="stylesheet" href="plugins/jquery-ui-1.10.3.custom/css/ui-lightness/jquery-ui-1.10.3.custom.css">*}
	</head>
	<body class="site com_content view-article no-layout no-task itemid-435">
		<div class="body">
			<div class="container">
					{include file="head.tpl"}
					{include file="categoryMenu.tpl"}
					{include file="main.tpl"}
			</div>
		</div>
		{include file="foot.tpl"}
	</body>
</html>
