{foreach from=array_merge($unit->getAttackPart()->getQuestions(), $unit->getDefencePart()->getQuestions()) item="activeTask" key="hash" name="tasks"}
	{assign var=index value=$smarty.foreach.tasks.index+1}
	<li class="{$activeTask|getStatusCssClass}">
		<a href="{$request->getUrlForTask($activeTask)}">{$index}. {include file="defineTaskLabel.tpl"}</a>
	</li>
{/foreach}