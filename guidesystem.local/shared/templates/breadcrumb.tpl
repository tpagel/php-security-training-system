<ul class="breadcrumb">
	<li class="active">
		<span class="divider icon-location hasTooltip" title="You are here: "></span>
	</li>
	{foreach from=$breadcrumbManager->getBreadcrumb() item="item" name="breadcrumb"}
	<li>
		<a class="pathway" href="">{$item}</a>
		{if !$smarty.foreach.breadcrumb.last}
		<span class="divider">
			<img alt="" src="images/arrow.png" />
		</span>
		{/if}
	</li>
	{/foreach}
</ul>