<ul class="nav menu nav-pills">
{foreach from=$categoryManager->getCategories() item=category}
	<li class="item-435 {if $category->isComplete()}complete{else if $category->isActive()}current active{/if}"><a href="{$request->getUrlForCategory($category)}" >{$category->getName()}</a></li>
{/foreach}
</ul>