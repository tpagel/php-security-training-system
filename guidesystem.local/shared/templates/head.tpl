<!-- Header -->
<header class="header" role="banner">
	<div class="header-inner clearfix">
		<a class="brand pull-left" href="{$request->getFullUrl()}">
			<span class="site-title" title="test">{block name="title"}{/block}</span> 
		</a>
		<div class="header-search pull-right"></div>
	</div>
</header>