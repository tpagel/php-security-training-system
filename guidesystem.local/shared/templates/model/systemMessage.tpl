{foreach from=$systemMessage->getInfoMessages() item="message" name="systemMessage"}
		<script>$.toast({$message|json_encode}, {literal}{{/literal}sticky: true, type: 'info'{literal}}{/literal});</script>
{/foreach}

{foreach from=$systemMessage->getNegativeMessages() item="message" name="systemMessage"}
		<script>$.toast({$message|json_encode}, {literal}{{/literal}sticky: false, type: 'danger'{literal}}{/literal});</script>
{/foreach}
{foreach from=$systemMessage->getPositiveMessages() item="message" name="systemMessage"}
		<script>$.toast({$message|json_encode}, {literal}{{/literal}sticky: false, type: 'success'{literal}}{/literal});</script>
{/foreach}