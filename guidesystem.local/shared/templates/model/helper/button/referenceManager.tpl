<img src="images/book.png" />
<button id="reference-button">Referenzen</button>
<div id="reference-message" title="Referenzen">
	<ul>
	{foreach from=$helper->getReferences() item="reference"}
		<li><a href="{$reference->getLink()}" target="__blank">{$reference->getTitle()}</a></li>
	{/foreach}
	</ul>
</div>
<script>
$(function() {
	$( "#reference-message" ).dialog({
		autoOpen: false,
		show: {
			effect: "blind",
			duration: 1000
		},
		hide: {
			effect: "explode",
			duration: 1000
		}
	});

	$( "#reference-button" ).click(function() {
		$( "#reference-message" ).dialog( "open" );
	});
});
 </script>