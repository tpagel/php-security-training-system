{extends file="model/task/taskBase.tpl"}
{block name="input"}
<h3>Abzusichernder Quellcode:</h3>
	<textarea name="answers[{$activeTask->getQuestion()|sha1}]" class="editSourceCode">{$activeTask->getBaseFileContent()}</textarea>
				<script>
				var myCodeMirror = CodeMirror.fromTextArea($('.editSourceCode')
						.get(0), {
					lineNumbers : true,
					matchBrackets : true,
					mode : "application/x-httpd-php",
					indentUnit : 4,
					indentWithTabs : true,
					enterMode : "keep",
					tabMode : "shift"
				});
				</script>
{/block}
