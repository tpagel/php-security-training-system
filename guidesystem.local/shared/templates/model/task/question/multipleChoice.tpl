{extends file="model/task/taskBase.tpl"}
{block name="taskHeader"}Multiple-Choice-Frage{/block}
{block name="input"}
	<fieldset>
	{foreach from=$activeTask->getCheckboxQuestions() item=checkboxQuestion key=checkboxIndex}
			<input type="hidden" name="answers[{$activeTask->getQuestion()|sha1}][{$checkboxIndex}]" value="0"/>
			<input type="checkbox" name="answers[{$activeTask->getQuestion()|sha1}][{$checkboxIndex}]" value="1"/ id="{$activeTask->getQuestion()|sha1}{$checkboxIndex}">
			<label  for="{$activeTask->getQuestion()|sha1}{$checkboxIndex}" >{$checkboxQuestion->getLabel()}</label>
			<br />
		{/foreach}
	</fieldset>
{/block}
