{extends file="model/partBase.tpl"}
{block name="task"}
	{$smarty.block.child}
	<h3>{block name="taskHeader"}Auftragsbeschreibung{/block}</h3>
	<div>{$activeTask->getQuestion()}</div>
	<div>
		<form action="" method="post">
			{block name="input"}Fehler{/block}
			<input type="submit" name="submit"/>
		</form>
	</div>
{/block}