{extends file="layout.tpl"}
{block name="title"}Lerneinheit: {$activeUnit->getName()}{/block}
{block name="subtitle"}Typ: {include file="defineTaskLabel.tpl"}{/block}
{block name="status"}
	<div
	{if $activeTask->isComplete()}
		class="statusComplete">Status: Absolviert
	{else}
		class="statusIncomplete">Status: Offen{/if}
	</div>
{/block}
{block name="main"}
	<div id="main">
		<div>
			{block name="task"}Es ist ein unerwarteter Fehler aufgetreten{/block}
			{*$smartyWrapper->getHtml($activeTask)*}
		</div>
	</div>
{/block}