<div class="row-fluid">
	<div id="aside" class="span3">
		<!-- Begin Left Sidebar -->
		<div class="well "><h3 class="page-header">Lerneinheiten:</h3>
			{include file="unitMenu.tpl"}
		</div>
		<!-- End Left Sidebar -->
	</div>
	<main id="content" role="main" class="span9">
		<!-- Begin Content -->
		<div class="moduletable">
			<div id="system-message-container">
				<div id="system-message">
				{$systemMessageHtml}
				</div>
			</div>
			<div class="item-page">
				<div class="page-header">
						{block name="subtitle"}Hier sollte der Untertitel stehen{/block}
						{block "status"}{/block}
						{assign var="difficulty" value=$activeTask->getDifficulty()}
						Schwierigkeitsgrad: {include file="difficulty/$difficulty.tpl"}
				</div>
				<div class="helpers">
					{foreach from=$activeTask->getHelpers() item="helper"}
						<span class="helper">
							{assign var="className" value=lcfirst($smartyWrapper->getClassNameFromObject($helper))}
							{include file="model/helper/button/$className.tpl"}
						</span>	
					{/foreach}
					
					{foreach from=$activeTask->getHelpers() item="helper"}
						<span class="helper">
							{assign var="className" value=lcfirst($smartyWrapper->getClassNameFromObject($helper))}
							{include file="model/helper/$className.tpl"}
						</span>	
					{/foreach}
				</div>
				{block name="main"}UNIT{/block}
			 </div> 
			{include file="breadcrumb.tpl"}
		</div>
		<!-- End Content -->
	</main>
</div>