<div id="leftcolumn">
			<ul>
				{assign var="category" value=$categoryManager->getActiveCategory()}
				{foreach from=$category->getUnits() item=unit}
					<li class="{$unit|getStatusCssClass}">
						<a href="?category={$category->getDirectory()}&unit={$unit->getDirectory()}">{$unit->getName()}</a>
					</li>
						{if $unit->isActive()}
							<ul>
								{include file="unitMenu_questions.tpl"}
							</ul>								
						{/if}
				{/foreach}
			</ul>
</div>