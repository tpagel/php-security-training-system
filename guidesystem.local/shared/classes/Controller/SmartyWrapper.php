<?php
namespace GuideSystem\Controller;
class SmartyWrapper {
	private $smarty;

	public function __construct(\Smarty &$smarty) {
		$this->smarty = $smarty;
	}

	public function getHtml($object, $templateFolder = "") {
		$className = get_class($object);

		$relativeClassPath = self::getPathForClass($className);
		$classNameWithoutNamespace = $this->getRemovedNamespaceFromClassName($className);
		$this->smarty->assign(lcfirst($classNameWithoutNamespace), $object);
		$template = $relativeClassPath . ".tpl";
		$templateFullPath = $this->getTemplatePath($template);
		$html = $this->smarty->fetch($templateFullPath);
		return $html;
	}

	public static function getPathForClass($className) {
		$classNameAndNamespace = explode("\\", $className);
		$className = "";
		$first = true;
		for ($i = 0; $i < count($classNameAndNamespace); $i++) {
			$element = $classNameAndNamespace[$i];
			$className = $className . "/" . lcfirst($element);
		}
		$className = str_replace("guideSystem", "", $className);
		$className = str_replace("unitSystem", "", $className);
		return $className;
	}

	private function getTemplatePath($template) {
		foreach ($this->smarty->getTemplateDir() as $templateDir) {
			$templateFullPath = $templateDir . "/" . $template;
			if (file_exists($templateFullPath)) {
				return $templateFullPath;
			}
		}
		throw new \GuideSystem\Exception\LogException("Template file not found in $templateFullPath");
	}

	public function getRemovedNamespaceFromClassName($className) {
		$classNameWithoutNamespace = preg_replace("/.*\\\/", "", $className);
		return $classNameWithoutNamespace;
	}

	public function getClassNameFromObject($object) {
		$className = get_class($object);
		return $this->getRemovedNamespaceFromClassName($className);
	}
}
