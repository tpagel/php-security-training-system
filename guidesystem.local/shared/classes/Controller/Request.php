<?php
namespace GuideSystem\Controller;
class Request {
	private $category = false;
	private $unit = false;
	private $part = false;
	private $question = false;

	public function __construct($get) {
		$this->set("unit", $get);
		$this->set("part", $get);
		$this->set("question", $get);
		$this->set("category", $get);
	}

	private function set($key, $data) {
		if (array_key_exists($key, $_GET)) {
			$this->$key = $_GET[$key];
		}
	}

	public function redirectTo(\GuideSystem\Model\Task\TaskBase $task) {
		$this->setTaskObject($task);
		$this->redirect();
	}
	
	/**
	 * Redirect to actual active unit of this object
	 */
	public function redirect() {
		$url = $this->getFullUrl();
		$this->redirectToUrl($url);
	}

	public function redirectToUrl($url) {
		header("location: $url");
		exit();
	}
	
	public function getFullUrl() {
		return "?category=" . $this->category . "&unit=" . $this->unit
		. "&part=" . $this->part . "&question=" . $this->question;
	}
	
	public function getUrlForCategory(\GuideSystem\Model\Category $category) {
		return "?category=" . $category->getDirectory();
	}
	
	public function getUrlForTask(\GuideSystem\Model\Task\TaskBase $task) {
		$request = new Request(array());
		$this->setTaskObject($task, $request);
		$url = $request->getFullUrl();
		return $url;
	}
	
	public function getCategory() {
		return $this->category;
	}
	
	public function getUnit() {
		return $this->unit;
	}
	
	public function getPart() {
		return $this->part;
	}
	
	public function getQuestion() {
		return $this->question;
	}

	public function setCategory($category) {
		$this->category = $category;
	}

	public function setUnit($unit) {
		$this->unit = $unit;
	}

	public function setPart($part) {
		$this->part = $part;
	}

	public function setTaskObject(\GuideSystem\Model\Task\TaskBase $task,
			Request $request = null) {
		if ($request == null) {
			$request = $this;
		}
		$nextPart = $task->getParentPart();
		$nextUnit = $nextPart->getParentUnit();
		$request
				->setCategory(
						$nextUnit->getParentCategory()->getDirectory());
		$request->setUnit($nextUnit->getDirectory());
		$request->setPart($nextPart->getPartName());
		$request->setQuestion(sha1($task->getQuestion()));
	}

	public function setQuestion($question) {
		$this->question = $question;
	}
}
