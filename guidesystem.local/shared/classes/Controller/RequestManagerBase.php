<?php
namespace GuideSystem\Controller;
class RequestManagerBase {
	const CATEGORY_SERIALISIZE_FILE = "categoryManager.xml";
	protected $categoryManager;

	/**
	 * Loads cache file
	 */
	protected function loadCategoryManager() {
		if (file_exists($this->getSerialiszeCategoryFilePath())) {
			$categoryManagerContent = file_get_contents($this->getSerialiszeCategoryFilePath());
			if ($categoryManagerContent === FALSE) {
				throw new LogException("Could not load CategoryManager from path " . $this->getSerialiszeCategoryFilePath());
			}
			$this->categoryManager = unserialize($categoryManagerContent);
		} else {
			$director = new Director();
			$this->categoryManager = $director->getCategoryManager();
		}
	}

	/**
	 * Stores cache file
	 */
	protected function serialisizeUtilities() {
		$serializedFileContent = serialize($this->categoryManager);
		if (file_put_contents($this->getSerialiszeCategoryFilePath(), $serializedFileContent) === FALSE) {
			throw new \GuideSystem\Exception\LogException("Could not serialisize CategoryManager");
		}
		\GuideSystem\Model\SystemMessage::serialisize();
	}

	private function getSerialiszeCategoryFilePath() {
		return CACHE_DIR . DIRECTORY_SEPARATOR . self::CATEGORY_SERIALISIZE_FILE;
	}
}
