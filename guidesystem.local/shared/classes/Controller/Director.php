<?php
namespace GuideSystem\Controller;
use GuideSystem\Model\Unit;
use GuideSystem\Model\Category;
class Director {
	const UNIT_DEFINITION_FILENAME = "unitDefinition.xml";
	const CATEGORY_DEFINITION_FILENAME = "categoryDefinition.xml";
	const UNIT_SUBDIR = "/units/";
	private $categorieManager;
	private $xmlBuilder;

	public function __construct() {
		$this->generateCategoriesAndUnits(UNIT_SHARED_DIR . "/../" . self::UNIT_SUBDIR);
	}
	
	/**
	 * Generates categories and adds each them to the categoryManager
	 */
	public function generateCategoriesAndUnits($categoriesDirectoryPath) {
		$this->categorieManager = new CategoryManager();
		$this->xmlBuilder = new XmlBuilder();
		foreach ($this->getDefinitionFiles($categoriesDirectoryPath, self::CATEGORY_DEFINITION_FILENAME) as $categoryDirectory => $categoryDefinitionFile) {
			$categoryPath = $categoriesDirectoryPath . $categoryDirectory . "/";
			$category = $this->generateCategory($categoryPath, $categoryDefinitionFile);
			$this->categorieManager->addCategory($category, $categoryDirectory);
		}
	}
	
	/**
	 * Generates a category with units
	 * @param string $categoryPath Path to file
	 * @param string $unitDefinitionFile Path to file
	 * */
	private function generateCategory($categoryPath, $categoryDefinitionFile) {
		$category = $this->createCategoryObject($categoryDefinitionFile);
		foreach ($this->getDefinitionFiles($categoryPath, self::UNIT_DEFINITION_FILENAME) as $unitDirectory => $unitDefinitionFile) {
			$unit = $this->createUnitObject($category, $unitDefinitionFile);
			$category->addUnit($unit);
		}
		return $category;
	}
	
	/**
	 * Creates an empty category
	 * @return Category
	 */
	private function createCategoryObject($categoryDefinitionFile) {
		$category = new \GuideSystem\Model\Category();
		$category = $this->xmlBuilder->xmlFileToObject($categoryDefinitionFile, $category);
		return $category;
	}
	
	/**
	 * Creates a empty unit
	 * @param Category $category
	 * @param string $unitDefinitionFile Path to file
	 * @return Unit
	 */
	private function createUnitObject($category, $unitDefinitionFile) {
		$unit = new \GuideSystem\Model\Unit($category);
		$unit = $this->xmlBuilder->xmlFileToObject($unitDefinitionFile, $unit);
		return $unit;
	}

	public function getDefinitionFiles($path, $definitonFilename) {
		$directoryContent = scandir($path);
		$definitionFiles = array();
		foreach ($directoryContent as $subDirectory) {
			if (!is_dir($path . $subDirectory)) {
				continue;
			}
			$definitionFile = $path . $subDirectory . '/' . $definitonFilename;
			if (!file_exists($definitionFile)) {
				\Logger::getLogger("main")->error("No definition for category at $definitionFile");
				continue;
			}
			$definitionFiles[$subDirectory] = $definitionFile;
		}
		return $definitionFiles;
	}

	public function getCategoryManager() {
		return $this->categorieManager;
	}
}
?>