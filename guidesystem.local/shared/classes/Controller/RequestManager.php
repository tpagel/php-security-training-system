<?php
namespace GuideSystem\Controller;
class RequestManager extends RequestManagerBase {
	private $request;
	private $activeTask;
	private $director;
	private $smarty;

	public function __construct(\Smarty $smarty) {
		$this->smarty = $smarty;
		$this->request = new Request($_GET);

		$this->loadCategoryManager();
		$this->setactiveTask();
		$this->loadUtilities();
		$this->display();
		$this->serialisizeUtilities();
	}

	private function setActiveTask() {
		$categoryName = $this->request->getCategory();
		if ($categoryName == false) {
			$unit = $this->categoryManager->getFirstUncompleteUnit();
			$this->setActiveTaskFromUnit($unit);
		} else {
			$unit = $this->getActiveUnit($categoryName);
			$this->setActiveTaskFromUnit($unit);
		}
	}

	private function getActiveUnit($categoryName) {
		$category = $this->categoryManager->getCategory($categoryName);
		if (!$category instanceof \GuideSystem\Model\Category) {
			throw new \GuideSystem\Exception\LogException("Category does not exists");
		}
		$unitName = $this->request->getUnit();
		if ($unitName == false) {
			$units = $category->getUnits();
			$unit = reset($units);
			if ($unit == false) {
				die("Alle Lerneinheiten wurden erfolgreich absolviert. Glückwunsch!");
			}
		} else {
			$unit = $category->getUnit($unitName);
		}
		if ($unit == false) {
			throw new \GuideSystem\Exception\LogException("Unit does not exists");
		}
		return $unit;
	}

	private function setActiveTaskFromUnit(\GuideSystem\Model\Unit $unit) {
		$partName = $this->request->getPart();
		if ($partName == false) {
			$part = $unit->getAttackPart();
		} else {
			$getPartNameMethodName = "get" . ucfirst($partName) . "Part";
			$part = $unit->$getPartNameMethodName();
		}
		if ($this->request->getQuestion() == false) {
			$this->activeTask = $part->getFirstUncompleteQuestion();
			if ($this->activeTask == false) {
				$questions = $part->getQuestions();
				$this->activeTask = reset($questions);
			}
		} else {
			$this->activeTask = $part->getQuestion($this->request->getQuestion());
		}
		$this->activeTask->setActive($this->categoryManager);
	}

	private function loadUtilities() {
		$breadcrumbManager = new BreadcrumbManager($this->activeTask);
		$systemMessage = \GuideSystem\Model\SystemMessage::singleton();
		if (isset($_POST['submit'])) {
			$activeUnit = $this->activeTask->getParentPart()->getParentUnit();
			$activeUnit->doPost($_POST['answers']);
			if ($this->activeTask->isComplete()) {
				$systemMessage->addPositiveMessage("Aufgabe erfolgreich beantwortet, automatische Weiterleitung zur Nächsten");
				$this->serialisizeUtilities();
				$this->redirectToNextQuestion();
			} else {
				$systemMessage->addInfoMessage("Aufgabe nicht erfolgreich beantwortet");
			}
		}
		$smartyWrapper = new SmartyWrapper($this->smarty);
		$this->smarty->assign('systemMessageHtml', $smartyWrapper->getHtml($systemMessage));
		$this->smarty->assign('smartyWrapper', $smartyWrapper);
		$this->smarty->assign('categoryManager', $this->categoryManager);
		$this->smarty->assign('activeUnit', $this->activeTask->getParentPart()->getParentUnit());
		$this->smarty->assign('breadcrumbManager', $breadcrumbManager);
		$this->smarty->assign('activeTask', $this->activeTask);
		$this->smarty->assign('request', $this->request);
		$helpers = $this->activeTask->getHelpers();
		$this->smarty->assign('helpers', $helpers);
	}

	private function redirectToNextQuestion() {
		$nextTask = $this->categoryManager->getNextUncompleteTask($this->activeTask);
		if ($nextTask == false) {
			$this->request->redirectToUrl("/complete.php");
		}
		$nextTask->setActive($this->categoryManager);
		$this->request->redirectTo($nextTask);
	}

	private function display() {
		$templatePath = SmartyWrapper::getPathForClass(get_class($this->activeTask));
		$absoluteTemplatePath = GUIDE_SHARED_DIR . "/templates/" . "$templatePath.tpl";
		$this->smarty->display($absoluteTemplatePath);
	}
}
