<?php
namespace GuideSystem\Controller;
/**
 * Note: An array contains classes, always
 */
class XmlBuilder {
	public function xmlFileToObject($xmlFileName, $object = null) {
		if (!file_exists($xmlFileName))
			die("XmlUtils::xmlFileToObject Error: $xmlFileName nonexistent!");
		$xml = simplexml_load_file($xmlFileName);
		return $this->xmlToObject($xml, $object);
	}
	
	private function xmlToObject($xml, $object) {
		$simpleTypeXmls = $this->getChildrenTypes($xml, "simple");
		$classTypeXmls = $this->getChildrenTypes($xml, "class");
		$arrayTypeXmls = $this->getChildrenTypes($xml, "array");
		$this->handleXmls($simpleTypeXmls, "simple", $object);
		$this->handleXmls($classTypeXmls, "class", $object);
		$this->handleXmls($arrayTypeXmls, "array", $object);
		return $object;
	}

	private function handleXmls($typeXmls, $type, $parentObject) {
		foreach ($typeXmls as $key => $xmls) {
			$setMethodName = "setType" . ucfirst($type);
			foreach ($xmls as $xml) {
				$attributeName = preg_replace("/.*_/", "", $key);
				$this->$setMethodName($xml, $parentObject, $attributeName);
			}
		}
	}

	private function setTypeSimple($xml, $parentObject, $attributeName) {
		$value = (string) $xml;
		$value = preg_replace("/\n/", " ", $value);
		$value = preg_replace("/\t/", " ", $value);
		$value = preg_replace("/\s+/", " ", $value); // Multiple whitespaces to one space
		$parentObject->$attributeName(trim($value));
	}

	private function setTypeClass($xml, $parentObject, $attributeName) {
		$className = "GuideSystem\Model\\" . ucfirst($attributeName);
		$object = new $className($parentObject);
		$object = $this->xmlToObject($xml, $object);
		$parentObject->$attributeName($object);
	}

	private function setTypeArray($xml, $parentObject, $attributeName) {
		$array = array();
		foreach ($xml->children() as $xmlChild) {
			$className = "GuideSystem\Model\\"
					. $this->getClassNameForTag($xmlChild->getName());
			$object = new $className($parentObject);
			$childObject = $this->xmlToObject($xmlChild, $object);
			$array[] = $object;
		}
		$parentObject->$attributeName($array);
	}

	private function getClassNameForTag($tag) {
		$classNameAndNamespace = explode("_", $tag);
		$className = "";
		$first = true;
		foreach ($classNameAndNamespace as $element) {
			if ($first) {
				$first = false;
				$className = $className . ucfirst($element);
			} else {
				$className = $className . "\\" . ucfirst($element);
			}
		}
		return $className;
	}

	private function getChildrenTypes($xml, $type) {
		$typeSimpleXmls = array();
		foreach ($xml->children() as $xmlChild) {
			$attributeCount = 0;
			foreach ($xmlChild->attributes() as $key => $attribute) {
				$attributeCount++;
				if ("type" != $key) {
					continue;
				}
				if ((string) $attribute == $type) {
					$typeSimpleXmls[$xmlChild->getName()][] = $xmlChild;
				}
			}
			if ($attributeCount == 0 && $type == "simple") {
				$typeSimpleXmls[$xmlChild->getName()][] = $xmlChild;
			}
		}
		return $typeSimpleXmls;
	}
}
