<?php
namespace GuideSystem\Controller;
class BreadcrumbManager {
	private $breadcrumb = array();

	public function __construct(\GuideSystem\Model\Task\TaskBase $task) {
		$activePart = $task->getParentPart();
		if ($activePart->getPartName() == "attack") {
			$name = "Angriff";
		} else {
			$name = "Abwehr";
		}
		array_unshift($this->breadcrumb, $name);
		$activeUnit = $activePart->getParentUnit();
		array_unshift($this->breadcrumb, $activeUnit->getName());
		$activeCategory = $activeUnit->getParentCategory();
		array_unshift($this->breadcrumb, $activeCategory->getName());
	}

	public function getBreadcrumb() {
		return $this->breadcrumb;
	}
}
