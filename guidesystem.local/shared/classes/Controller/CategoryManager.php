<?php
namespace GuideSystem\Controller;
use GuideSystem\Model\Task\TaskBase;
class CategoryManager {
	const CATEGORY_SORT_FILE = "categorySortOrder.txt";
	private $categories = array();

	public function addCategory(\GuideSystem\Model\Category $category, $categoryDirectory) {
		$this->categories[$categoryDirectory] = $category;
	}

	public function getCategories() {
		return $this->categories;
	}

	public function getCategory($directory) {
		if (!array_key_exists($directory, $this->categories)) {
			throw new \GuideSystem\Exception\LogException("Category does not exists");
		}
		return $this->categories[$directory];
	}

	public function addUnit($categoryName, Unit $unit) {
		if (!array_key_exists($categoryName, $this->categories)) {
			throw new \GuideSystem\Exception\LogException("Category does not exists");
		}
		$this->categories[$categoryName]->addUnit($unit);
	}

	public function getActiveCategory() {
		foreach ($this->categories as $category) {
			if ($category->isActive()) {
				return $category;
			}
		}
		return false;
	}

	public function setAllTasksInactive() {
		foreach ($this->categories as $category) {
			foreach ($category->getUnits() as $unit) {
				foreach ($unit->getAttackPart()->getQuestions() as $question) {
					$question->isActive(false);
				}
				foreach ($unit->getDefencePart()->getQuestions() as $question) {
					$question->isActive(false);
				}
			}
		}
	}

	public function getUnit($unitDirectory) {
		foreach ($this->categories as $category) {
			if (($unit = $category->getUnit($unitDirectory)) !== false) {
				return $unit;
			}
		}
		return false;
	}

	public function getFirstUncompleteUnit($startCategory = null) {
		if ($startCategory == null) {
			$isStartCategoryFound = true;
		} else {
			$isStartCategoryFound = false;
		}
		foreach ($this->categories as $category) {
			if (!$category->isComplete() && ($isStartCategoryFound)) {
				foreach ($category->getUnits() as $unit) {
					if (!$unit->isComplete()) {
						return $unit;
					}
				}
			}
			if (!$isStartCategoryFound && ($category->getDirectory() == $startCategory->getDirectory())) {
				$isStartCategoryFound = true;
			}
		}
		return false;
	}

	public function getNextUncompleteTask(\GuideSystem\Model\Task\TaskBase $startTask) {
		$part = $startTask->getParentPart();
		$actualUnit = $part->getParentUnit();
		$task = $this->getNextUncompleteTaskFromUnit($actualUnit, $startTask);
		if ($task !== false) {
			return $task;
		}
		$actualCategory = $actualUnit->getParentCategory();
		$nextUnit = $this->getNextUncompleteUnitFromCategory($actualUnit);
		if ($nextUnit != false) {
			return $this->getNextUncompleteTaskFromUnit($nextUnit);
		}
		$nextUnit = $this->getNextUncompleteUnitFromCategory($actualUnit);
		if ($nextUnit != false) {
			return $nextUnit;
		}
		//category done, next category
		$nextUnit = $this->getFirstUncompleteUnit($actualCategory);
		if ($nextUnit == false) {
			return false; //all done
		}
		$task = $this->getNextUncompleteTaskFromUnit($nextUnit);
		if ($task != false)
			return $task;
		throw new \GuideSystem\Exception\LogException("Could not find a task");
	}

	private function getNextUncompleteUnitFromCategory(\GuideSystem\Model\Unit $startUnit) {
		$category = $startUnit->getParentCategory();
		$found = false;
		foreach ($category->getUnits() as $unit) {
			if ($found) {
				return $unit;
			}
			if ($unit == $startUnit) {
				$found = true;
			}
		}
		return false;
	}

	private function getNextCategory(\GuideSystem\Model\Category $startCategory) {
		$isStartCategoryFound = false;
		foreach ($this->categories as $category) {
			if (!$category->isComplete() && $isStartCategoryFound) {
				return $category;
			}
			if (!$isStartCategoryFound && ($category->getDirectory() == $startCategory->getDirectory())) {
				$isStartCategoryFound = true;
			}
		}
		return false;
	}

	private function getNextUncompleteTaskFromUnit(\GuideSystem\Model\Unit $unit, \GuideSystem\Model\Task\TaskBase $startTask = null) {
		if ($startTask == null) {
			$taskFound = true;
		} else {
			$taskFound = false;
		}
		$tasks = array_merge($unit->getAttackPart()->getQuestions(), $unit->getDefencePart()->getQuestions());
		foreach ($tasks as $hash => $task) {
			if ($taskFound) {
				if (!$task->isComplete()) {
					return $task;
				}
			} else {
				if ($hash == sha1($startTask->getQuestion())) {
					$taskFound = true;
				}
			}
		}
		return false;
	}
}
