<?php
namespace GuideSystem\Exception;
class LogException extends \Exception {
	private $errorType;
	public function __construct($message, $errorType = 'error') {
		parent::__construct($message);
		$this->errorType = $errorType;
		$logger = \Logger::getLogger("main");
		$logger->$errorType($this->__toString());
	}
	public function __toString() {
		return __CLASS__
				. "[file: {$this->file}] [line: {$this->line}]: {$this->message}\nSTART-TRACE:\n{$this->getTraceAsString()}\nEND_TRACE";
	}
}