<?php
namespace GuideSystem\Model;
class SystemMessage {
	const CACHE_FILE_NAME = "SystemMessage.xml";
	private static $instance;
	private $positiveMessages = array();
	private $negativeMessages = array();
	private $infoMessages = array();

	public static function singleton() {
		if (!isset(self::$instance)) {
			$className = __CLASS__;
			$filename = CACHE_DIR . DIRECTORY_SEPARATOR . $className . ".xml";
			//if (file_exists($filename)) unlink($filename);// For fast testing with out cache
			if (file_exists($filename)) {
				$fileContent = file_get_contents($filename);
				self::$instance = unserialize($fileContent);
			} else {
				self::$instance = new $className();
			}
		}
		return self::$instance;
	}

	private function __construct() {
	}

	public function __clone() {
		trigger_error('Clonen ist nicht erlaubt.', E_USER_ERROR);
	}

	public static function serialisize() {
		$className = __CLASS__;
		$filename = CACHE_DIR . DIRECTORY_SEPARATOR . $className . ".xml";
		$fileContent = serialize(self::$instance);
		file_put_contents($filename, $fileContent);
	}

	public function addPositiveMessage($message) {
		$this->positiveMessages[] = $message;
	}

	public function addNegativeMessage($message) {
		$this->negativeMessages[] = $message;
	}

	public function addInfoMessage($message) {
		$this->infoMessages[] = $message;
	}

	public function getPositiveMessages() {
		$messages = $this->positiveMessages;
		unset($this->positiveMessages);
		return $messages;
	}

	public function getNegativeMessages() {
		$messages = $this->negativeMessages;
		unset($this->negativeMessages);
		return $messages;
	}

	public function getInfoMessages() {
		$messages = $this->infoMessages;
		unset($this->infoMessages);
		return $messages;
	}
}
