<?php

namespace GuideSystem\Model;
class Category {
	use \GuideSystem\TraitElement\Builder;
	private $units = array();
	private $directory;
	private $name;
	private $helpers = array();

	public function __construct($name = null) {
		if ($name != null)
			$this->name = $name;
	}

	public function addUnit(Unit $unit) {
		$newUnits = array();
		$isNewUnitInserted = false;
		if (empty($this->units)) {
			$newUnits[$unit->getDirectory()] = $unit;
		} else {
			$newUnits = $this->getNewSort($unit);
		}
		$this->units = $newUnits;
	}

	private function getNewSort($newUnit) {
		$newUnits = array();
		$inseredUnitOnLastPosition = true;
		foreach ($this->units as $key => &$existingUnit) {
			if ($existingUnit->getSortLevel() > $newUnit->getSortLevel()) {
				$newUnits[$newUnit->getDirectory()] = $newUnit;
				$inseredUnitOnLastPosition = false;
			}
			$newUnits[$existingUnit->getDirectory()] = $existingUnit;
		}
		if ($inseredUnitOnLastPosition) {
			$newUnits[$newUnit->getDirectory()] = $newUnit;
		}
		return $newUnits;
	}

	public function isActive() {
		foreach ($this->units as $unit) {
			if ($unit->isActive()) {
				return true;
			}
		}
		return false;
	}

	public function getFirstUncompleteUnit() {
		foreach ($this->units as $unit) {
			if (!$unit->isComplete()) {
				return $unit;
			}
		}
		return false;
	}

	public function isComplete() {
		foreach ($this->units as $unit) {
			if (!$unit->isComplete()) {
				return false;
			}
		}
		return true;
	}

	public function getName() {
		return $this->name;
	}

	public function getUnit($directory) {
		if (array_key_exists($directory, $this->units)) {
			return $this->units[$directory];
		} else {
			return false;
		}
	}

	public function getUnits() {
		return $this->units;
	}

	public function getActiveUnit() {
		foreach ($this->units as $unit) {
			if ($unit->isActive()) {
				return $unit;
			}
		}
		return false;
	}

	public function getFirstUnit() {
		foreach ($this->units as $unit) {
			return $unit;
		}
	}

	public function getDirectoryPath() {
		$path = UNIT_SYSTEM_HTDOCS_DIR . "/../units/" . $this->directory;
		return $path;
	}

	public function getDirectory() {
		return $this->directory;
	}

	public function getHelpers() {
		return $this->helpers;
	}
}
