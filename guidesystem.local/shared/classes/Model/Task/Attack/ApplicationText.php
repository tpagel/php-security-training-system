<?php
namespace GuideSystem\Model\Task\Attack;
class ApplicationText extends \GuideSystem\Model\Task\Question\Text {
	use \GuideSystem\TraitElement\Builder;
	private $instruction;

	private $type;
	private $isApplication = true;
	private $applicationName = "Verwundbare Anwendung";

	public function __construct(\GuideSystem\Model\PartBase $parent) {
		parent::__construct($parent);
	}

	public function getSourceCodePath() {
		return $this->getDirectoryPath() . \PartManagement::PROCESS_FILE_NAME;
	}

	public function getSourceCode() {
		return $this->sourceCode;
	}

	public function getApplicationName() {
		return $this->applicationName;
	}

	public function getInstruction() {
		return $this->instruction;
	}
}
