<?php
namespace GuideSystem\Model\Task\Question;
class CheckboxQuestion {
	use \GuideSystem\TraitElement\Builder;
	private $isValid = false;
	private $label;
	private $isComplete = false;

	public function getLabel() {
		return $this->label;
	}

	public function doPost($post) {
		$this->isComplete = ((bool) $post == $this->isValid);
	}
	public function isComplete($bool = null) {
		if ($bool === null) {
			return $this->isComplete;
		} else {
			$this->isComplete = $bool;
		}
	}
}
