<?php
namespace GuideSystem\Model\Task\Question;

class MultipleChoice extends \GuideSystem\Model\Task\TaskBase {
	use \GuideSystem\TraitElement\Builder;
	private $checkboxQuestions = array();

	public function getCheckboxQuestions() {
		return $this->checkboxQuestions;
	}

	public function isCorrectAnswered() {
		return true;
	}

	public function doPost($data) {
		$this->isComplete = true;
		foreach ($this->checkboxQuestions as $checkboxQuestionIndex => &$checkboxQuestion) {
			$checkboxQuestion->doPost($data[$checkboxQuestionIndex]);
			if (!$checkboxQuestion->isComplete()) {
				$this->isComplete = false;
			}
		}
	}
}
