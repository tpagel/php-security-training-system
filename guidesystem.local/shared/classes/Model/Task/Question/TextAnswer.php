<?php

namespace GuideSystem\Model\Task\Question;

class TextAnswer
{
    use \GuideSystem\TraitElement\Builder;

    private $answer = "";

    public function getAnswer()
    {
        return $this->answer;
    }

    public function __toString()
    {
        return $this->getAnswer();
    }

    public function equals($data)
    {
        $data = str_replace("'", "\"", $data);
        $answer = str_replace("'", "\"", $this->answer);
        return $data == $answer;
    }
}
