<?php
namespace GuideSystem\Model\Task\Question;
class Text extends \GuideSystem\Model\Task\TaskBase {
	use \GuideSystem\TraitElement\Builder;
	private $type;
	protected $answer = "";
	protected $answers = array();

	public function getQuestion() {
		return $this->question;
	}

	public function doPost($data) {
		if (!empty($this->answer) && !empty($this->answers)) {
			throw new \GuideSystem\Exception\LogException(
					"It is not allowed to set answer ans answers for one task");
		}
		if (!empty($this->answers)) {
			foreach ($this->answers as $answer) {
				if ($answer->equals($data)) {
					$this->isComplete = true;
					return;
				}
			}
		} else {
			$this->isComplete = ($this->answer == $data);
		}
	}
}
