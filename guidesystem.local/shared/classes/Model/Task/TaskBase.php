<?php
namespace GuideSystem\Model\Task;
class TaskBase {
	protected $isActive = false;
	protected $isComplete = false;
	protected $question;
	protected $hint = false;
	protected $parent;
	protected $solution = false;
	protected $helpers = array();
	protected $combinedHelpers = false;
	protected $difficulty = "medium";

	public function __construct(\GuideSystem\Model\PartBase $parentPart) {
		$this->parent = $parentPart;
	}

	public function getQuestion() {
		return $this->question;
	}

	public function setActive(\GuideSystem\Controller\CategoryManager &$categoryManager) {
		$categoryManager->setAllTasksInactive();
		$this->isActive(true);
	}

	public function getHint() {
		return $this->hint;
	}

	public function getParentPart() {
		return $this->parent;
	}

	public function getSolution() {
		return $this->solution;
	}

	public function getDirectoryPath() {
		return $this->parent->getDirectoryPath();
	}

	public function getCombinedHelpers() {
		if ($this->combinedHelpers === false) {
			$parentHelpers = $this->parent->getHelpers();
			$helpers = array_merge($parentHelpers, $this->helpers);
			$this->combinedHelpers = array_reverse(
					$this->combineHelpers($helpers));
		}
		return $this->combinedHelpers;
	}

	public function getHelpers() {
		return $this->getCombinedHelpers();
	}

	private function combineHelpers($helpers) {
		$combineTypes = array("GuideSystem\Model\Helper\ReferenceManager");
		$first = array();
		foreach ($helpers as $key => $helper) {
			$class = get_class($helper);
			if (in_array($class, $combineTypes)) {
				if (array_key_exists($class, $first)) {
					$first[$class]->add($helper);
					unset($helpers[$key]);
				} else {
					$first[$class] = $helper;
				}
			}
		}
		return $helpers;
	}

	public function getDifficulty() {
		return $this->difficulty;
	}
	public function isActive($bool = null) {
		if ($bool === null) {
			return $this->isActive;
		} else {
			$this->isActive = $bool;
		}
	}
	public function isComplete($bool = null) {
		if ($bool === null) {
			return $this->isComplete;
		} else {
			$this->isComplete = $bool;
		}
	}
}
