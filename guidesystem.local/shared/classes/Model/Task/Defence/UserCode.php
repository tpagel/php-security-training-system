<?php
// MOD: Remove unused comment, remove unused function setFilePath
namespace GuideSystem\Model\Task\Defence;
class UserCode extends \GuideSystem\Model\Task\TaskBase {
	use \GuideSystem\TraitElement\Builder;
	const BASE_FILE_NAME = 'processFile.php';
	const APPROACH_FILE_NAME = 'approach.php';
	private $baseFileContent;
	private $approachFileContentCanonized;

	public function __construct($parentPart) {
		parent::__construct($parentPart);
		$unit = $parentPart->getParentUnit();

		$baseFilePath = $unit->getDirectoryPath() . "/attack/" . self::BASE_FILE_NAME;
		$this->baseFileContent = file_get_contents($baseFilePath);

		$approachFilePath = $unit->getDirectoryPath() . "/defence/" . self::APPROACH_FILE_NAME;
		$this->approachFileContentCanonized = $this->getCanonizedFile($approachFilePath);
	}

	private function getCanonizedFile($path) {
		if (!file_exists($path)) {
			return "";
		}
		$content = file_get_contents($path);
		if ($content === false) {
			return "";
		}
		$content = $this->canonizeString($content);
		return $content;
	}
	// Old name: canonisizeString
	private function canonizeString($string) {
		return preg_replace("/\s/", "", $string);
	}

	public function doPost($data) {
		$this->baseFileContent = $data;
		$this->isComplete = ($this->canonizeString($data) == $this->approachFileContentCanonized);
	}

	public function getBaseFileContent() {
		return $this->baseFileContent;
	}
}
