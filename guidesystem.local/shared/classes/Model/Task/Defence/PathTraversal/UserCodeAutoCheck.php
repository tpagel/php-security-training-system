<?php
namespace {
// To be used in the unitsystem from the user
class UnsafeFileInclusionException extends \GuideSystem\Exception\LogException {
}
}
namespace GuideSystem\Model\Task\Defence\PathTraversal {
use GuideSystem\Model\Task\Defence\UserCode;
class UserCodeAutoCheck extends UserCode {
	use \GuideSystem\TraitElement\Builder;
	private $targetDirectoryPath;
	private $userEditedFilePath;
	private $baseFileContent; // Save user entered source code

	public function __construct(\GuideSystem\Model\PartBase $parentPart) {
		parent::__construct($parentPart);
		$unit = $parentPart->getParentUnit();
		if (!$this->copyDirectory($unit)) {
			throw new \GuideSystem\Exception\LogException("Could not copy a file");
		}
		$this->userEditedFilePath = $this->targetDirectoryPath . DIRECTORY_SEPARATOR . UserCode::BASE_FILE_NAME;
		$this->setBaseFileContent();
	}

	private function setBaseFileContent() {
		$this->baseFileContent = file_get_contents($this->userEditedFilePath);
	}

	private function copyDirectory(\GuideSystem\Model\Unit $unit) {
		$this->targetDirectoryPath = CACHE_DIR . $unit->getDirectory() . "UserCodeAutoCheck";
		$sourceDirectoryPath = $unit->getDirectoryPath() . "/attack/";
		$isCopy = $this->copy_r($sourceDirectoryPath, $this->targetDirectoryPath);
		if (!$isCopy)
			return false;
		$hiddenFilePath = $unit->getDirectoryPath() . "/defence/";
		$isCopy = $this->copy_r($hiddenFilePath, $this->targetDirectoryPath);
		return $isCopy;
	}

	private function copy_r($path, $dest) {
		if (is_dir($path)) {
			@mkdir($dest);
			$objects = scandir($path);
			if (sizeof($objects) > 0) {
				foreach ($objects as $file) {
					if ($file == "." || $file == "..")
						continue;
					// go on
					if (is_dir($path . DS . $file)) {
						$this->copy_r($path . DS . $file, $dest . DS . $file);
					} else {
						copy($path . DS . $file, $dest . DS . $file);
					}
				}
			}
			return true;
		} elseif (is_file($path)) {
			return copy($path, $dest);
		} else {
			return false;
		}
	}

	public function doPost($data) {
		file_put_contents($this->userEditedFilePath, $data);
		$this->baseFileContent = $data;
		ob_start();
		$this->isComplete = true;

		if (!$this->isSyntaxCorrect()) {
			$this->isComplete = false;
		} else if ($this->isInclusionOfForbiddenFilePossible()) {
			$this->isComplete = false;
		} else if (!$this->isInclusionOfAllowedFilePossible()) {
			$this->isComplete = false;
		}
		ob_end_clean();
		return $this->isComplete;
	}
	
	/**
	 * @see http://php.net/manual/ro/function.php-check-syntax.php
	 */
	private function isSyntaxCorrect() {
		$code = file_get_contents($this->userEditedFilePath);
		$code = str_replace("<?php", "", $code);
	
		// Unbalanced braces would break the eval in isSyntaxCorrect()
		if (!$this->isCorrectBalanceForBraces($code)) {
			return false;
		}
	
		$code = eval('if(0){' . $code . '}'); // Put $code in a dead code sandbox to prevent its execution
		if(false === $code) {
			$systemMessage = \GuideSystem\Model\SystemMessage::singleton();
			$systemMessage->addInfoMessage("Der eingegebene Quellcode enthält syntaktische Fehler.");
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * This is not trivial due to variable interpolation. which occurs in heredoc, backticked and double quoted strings
	 *
	 */
	private function isCorrectBalanceForBraces($code) {
		$braces = 0;
		$inString = 0;
		foreach (token_get_all('<?php ' . $code) as $token) {
			if (is_array($token)) {
				switch ($token[0]) {
					case T_CURLY_OPEN:
					case T_DOLLAR_OPEN_CURLY_BRACES:
					case T_START_HEREDOC:
						++$inString;
						break;
					case T_END_HEREDOC:
						--$inString;
						break;
				}
			} else if ($inString & 1) {
				switch ($token) {
					case '`':
					case '"':
						--$inString;
						break;
				}
			} else {
				switch ($token) {
					case '`':
					case '"':
						++$inString;
						break;
					case '{':
						++$braces;
						break;
					case '}':
						if ($inString)
							--$inString;
						else {
							--$braces;
							if ($braces < 0)
								return false;
						}
						break;
				}
			}
		}
		if ($braces) {
			return false;
		} else {
			return true;
		}
	}

	private function isInclusionOfForbiddenFilePossible() {
		$systemMessage = \GuideSystem\Model\SystemMessage::singleton();
		if ($this->isUnsafeFileInclusionExceptionForStock()) {
			$systemMessage->addInfoMessage("Die Datei <em>stock.php</em> kann weiterhin eingebunden werden oder es wird nicht der richtige Ausnahmetyp verwendet.");
			return true;
		}
		if (!$this->isExceptionForHiddenFiles()) {
			$systemMessage->addInfoMessage("Es können weiterhin unkontrolliert Dateien eingebunden werden.");
			return true;
		}
		return false;
	}

	private function isUnsafeFileInclusionExceptionForStock() {
		$_GET['language'] = "stock";
		try {
			include($this->userEditedFilePath);
		} catch (\UnsafeFileInclusionException $e) {
			return false;
		} catch (\Exception $e) {
		}
		return true;
	}

	private function isExceptionForHiddenFiles() {
		$_GET['language'] = "hiddenExceptionFile";
		try {
			include($this->userEditedFilePath);
		} catch (\UnsafeFileInclusionException $e) {
			// This is excpected, if the user code is valid
			return true;
		} catch (\Exception $e) {
		}
		return false;
	}

	private function isInclusionOfAllowedFilePossible() {
		$systemMessage = \GuideSystem\Model\SystemMessage::singleton();
		if (!$this->isLanguageIncludeable("de")) {
			$systemMessage->addInfoMessage("Bei der Einbindung der Sprache 'de' ist ein unerwarteter Fehler aufgetreten.");
			return false;
		}
		if (!$this->isLanguageIncludeable("en")) {
			$systemMessage->addInfoMessage("Bei der Einbindung der Sprache 'en' ist ein unerwarteter Fehler aufgetreten.");
			return false;
		}
		return true;
	}

	private function isLanguageIncludeable($language) {
		$_GET['language'] = $language;
		try {
			include($this->userEditedFilePath);
			if (isset($translation) && array_key_exists("data", $translation)) {
				return true;
			}
		} catch (\Exception $e) {
			return false;
		}
		return false;
	}

	public function getBaseFileContent() {
		return $this->baseFileContent;
	}

	public function getSolution() {
		return $this->solution;
	}
}
}
