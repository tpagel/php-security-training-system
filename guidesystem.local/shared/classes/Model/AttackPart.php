<?php
namespace GuideSystem\Model;
class AttackPart extends PartBase {
	use \GuideSystem\TraitElement\Builder;
	private $sourceCode;
	private $isSourceCodeViewable = false;
	protected $partName = "attack";
}
