<?php
namespace GuideSystem\Model;

class InvalidTypeException extends \GuideSystem\Exception\LogException {
}
class Unit {
	use \GuideSystem\TraitElement\Builder;
	private $name;
	private $sortLevel;
	private $directory;
	private $attackPart;
	private $defencePart;
	private $parent;
	private $helpers = array();

	public function __construct(Category $category) {
		$this->parent = $category;
	}

	public function doPost($data) {
		$activePart = $this->getActivePart();
		$activePart->doPost($data);
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function setCategory($category) {
		$this->category = $category;
	}

	public function setSortLevel($sortLevel) {
		$this->sortLevel = $sortLevel;
	}

	public function setAttackPart(UnitAttackPart $attackPart) {
		$this->attackPart = $attackPart;
	}

	public function setDefencePart(UnitDefencePart $defencePart) {
		$this->defencePart = $defencePart;
	}

	public function getName() {
		return $this->name;
	}

	public function getCategory() {
		return $this->category;
	}

	public function getSortLevel() {
		return $this->sortLevel;
	}

	public function getDirectory() {
		return $this->directory;
	}

	public function getAttackPart() {
		return $this->attackPart;
	}

	public function getDefencePart() {
		return $this->defencePart;
	}

	public function isActive() {
		if ($this->attackPart->isActive() || $this->defencePart->isActive()) {
			return true;
		} else {
			return false;
		}
	}

	public function isComplete() {
		if ($this->attackPart->isComplete() && $this->defencePart->isComplete()) {
			return true;
		} else {
			return false;
		}
	}

	public function getActivePart() {
		if ($this->attackPart->isActive())
			return $this->attackPart;
		if ($this->defencePart->isActive())
			return $this->defencePart;
		return false;
	}

	public function getDirectoryPath() {
		return $this->parent->getDirectoryPath() . "/" . $this->directory;
	}

	public function getParentCategory() {
		return $this->parent;
	}

	public function getFirstUncompletePart() {
		if (!$this->getAttackPart()->isComplete()) {
			return $this->attackPart;
		} else if (!$this->getDefencePart()->isComplete()) {
			return $this->defencePart;
		}
		return false;
	}

	public function getHelpers() {
		$parentHelpers = $this->parent->getHelpers();
		$helpers = array_merge($parentHelpers, $this->helpers);
		return array_reverse($helpers);
	}
}
