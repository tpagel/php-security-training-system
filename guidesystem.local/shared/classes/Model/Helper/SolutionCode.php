<?php
namespace GuideSystem\Model\Helper;
class SolutionCode extends HelperBase {
	use \GuideSystem\TraitElement\Builder;
	private $code;

	public function solutionFilePath($relativeFilepath) { // Called method for XMLBuilder
		$filepath = $this->getParentTask()->getDirectoryPath() . "/"
				. $relativeFilepath;
		$this->code = file_get_contents($filepath);
		if ($this->code === false) {
			throw new \GuideSystem\Exception\LogException("Could not find file in $filepath");
		}
	}

	public function getCode() {
		return $this->code;
	}
}
