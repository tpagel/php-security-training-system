<?php
namespace GuideSystem\Model\Helper;
class Reference extends HelperBase {
	use \GuideSystem\TraitElement\Builder;
	private $link;
	private $title;

	public function getLink() {
		return $this->link;
	}

	public function getTitle() {
		return $this->title;
	}
}
