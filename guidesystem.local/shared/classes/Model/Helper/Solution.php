<?php
namespace GuideSystem\Model\Helper;
class Solution extends HelperBase {
	use \GuideSystem\TraitElement\Builder;
	private $solution; // As HTML

	public function getSolution() {
		return $this->solution;
	}
}
