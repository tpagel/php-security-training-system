<?php
namespace GuideSystem\Model\Helper;
class Hint extends HelperBase {
	use \GuideSystem\TraitElement\Builder;
	private $hint;

	public function getHint() {
		return $this->hint;
	}
}
