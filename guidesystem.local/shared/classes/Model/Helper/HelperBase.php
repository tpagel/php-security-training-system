<?php
namespace GuideSystem\Model\Helper;
class HelperBase {
	private $parent;

	public function __construct($parentTask) {
		$this->parent = $parentTask;
	}

	public function getParentTask() {
		return $this->parent;
	}
}
?>