<?php
namespace GuideSystem\Model\Helper;
class ReferenceManager extends HelperBase {
	use \GuideSystem\TraitElement\Builder;
	private $references = array();

	public function getReferences() {
		return $this->references;
	}

	public function add(ReferenceManager $manager) {
		$this->references = array_merge($manager->getReferences(),
				$this->references);
	}
}
