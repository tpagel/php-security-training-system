<?php
namespace GuideSystem\Model;
class PartBase {
	protected $instruction;
	protected $hint;
	protected $questions = array();
	protected $parent;
	protected $helpers = array();

	public function __construct($parent) {
		$this->parent = $parent;
	}

	public function setInstruction($instruction) {
		$this->instruction = $instruction;
	}

	public function setHint($hint) {
		$this->hint = $hint;
	}

	public function questions($questions) {
		$questions = $this->getQuestionsWithHashKey($questions);
		$this->questions = $questions;
	}

	private function getQuestionsWithHashKey($questions) {
		$newQuestions = array();
		foreach ($questions as &$question) {
			$newQuestions[sha1($question->getQuestion())] = $question;
		}
		return $newQuestions;
	}

	public function isActive() {
		foreach ($this->questions as $question) {
			if ($question->isActive())
				return true;
		}
		return false;
	}

	public function doPost($data) {
		foreach ($data as $answerKey => $answer) {
			if (!array_key_exists($answerKey, $this->questions)) {
				continue;
			}
			$this->questions[$answerKey]->doPost($answer);
		}
	}

	public function isComplete() {
		foreach ($this->questions as $question) {
			if (!$question->isComplete()) {
				return false;
			}
		}
		return true;
	}

	public function getactiveTask() {
		foreach ($this->questions as $question) {
			if ($question->isActive()) {
				return $question;
			}
		}
		return false;
	}

	public function getPartName() {
		return $this->partName;
	}

	public function getDirectoryPath() {
		return realpath(
				$this->parent->getDirectoryPath() . "/" . $this->partName)
				. "/";
	}

	public function getParentUnit() {
		return $this->parent;
	}

	public function getFirstUncompleteQuestion() {
		foreach ($this->getQuestions() as $hash => $question) {
			if (!$question->isComplete()) {
				return $question;
			}
		}
		return false;
	}

	public function getFirstUncompleteQuestionHash() {
		foreach ($this->getQuestions() as $hash => $question) {
			if (!$question->isComplete()) {
				return $hash;
			}
		}
		return false;
	}

	public function getHelpers() {
		$parentHelpers = $this->parent->getHelpers();
		$helpers = array_merge($parentHelpers, $this->helpers);
		return array_reverse($helpers);
	}

	public function getQuestion($questionHash) {
		return $this->questions[$questionHash];
	}
	
	public function getInstruction() {
		return $this->instruction;
	}
	
	public function getHint() {
		return $this->hint;
	}
	
	public function getQuestions() {
		return $this->questions;
	}
}
