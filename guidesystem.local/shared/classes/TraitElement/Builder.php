<?php
namespace GuideSystem\TraitElement;
trait Builder {

	function __call($propertyName, $arguments) {
		if (!property_exists(get_class($this), $propertyName)) {
			throw new \Guidesystem\Model\Exception(
					"Property $name does not exists for class " . __CLASS__);
		}

		if (is_array($arguments[0])) {
			$this->$propertyName = array_merge($this->$propertyName, $arguments[0]);
		} else {
			$this->$propertyName = $this->typeCastAttribute($arguments[0]);
		}
		return $this;
	}
	private function typeCastAttribute($attributeValue) {
		if (is_string($attributeValue)) {
			if ($attributeValue == 'true') {
				return true;
			} else if ($attributeValue == 'false'){
				return false;
			}
		}
		return $attributeValue;
	}
}
?>