<?php
define('GUIDE_HTDOCS_DIR', realpath(dirname(__FILE__)) . "/../../htdocs/");
define('UNIT_SYSTEM_HTDOCS_DIR', realpath(dirname(__FILE__)) . "/../../../unitsystem.local/htdocs/");

define('GUIDE_SHARED_DIR', realpath(GUIDE_HTDOCS_DIR . "/../shared/"));
define('UNIT_SHARED_DIR', realpath(UNIT_SYSTEM_HTDOCS_DIR . "/../shared/"));

define('SMARTY_TEMPLATE_DIR', GUIDE_SHARED_DIR . '/templates/');
define('SMARTY_TEMPLATEC_DIR', realpath(GUIDE_HTDOCS_DIR . '/../templates_c/'));

define('CACHE_DIR', GUIDE_HTDOCS_DIR . '/../cache/');

define('UNIT_SERIALIZE_FILE', CACHE_DIR . 'units.xml');