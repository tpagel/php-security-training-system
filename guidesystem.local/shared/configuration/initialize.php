<?php
$smarty = new Smarty();
$smarty->setTemplateDir(SMARTY_TEMPLATE_DIR);
$smarty->setCompileDir(SMARTY_TEMPLATEC_DIR);
$smarty->force_compile = true;
// $smarty->debugging = true;
$smarty->caching = false;
$smarty->force_compile = true;
$smarty->cache_lifetime = 120;

function getFileRelativePathForClass($className) {
	$className = ltrim($className, "\\");
	$fileName = '';
	$namespace = '';
	if ($lastNsPos = strripos($className, "\\")) {
		$namespace = substr($className, 0, $lastNsPos);
		$className = substr($className, $lastNsPos + 1);
		$fileName = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR . ucfirst($className);
	} else {
		$fileName = ucfirst($className);
	}
	return $fileName;
}

function my_autoloader($className) {
	$fileName = getFileRelativePathForClass($className);
	if (preg_match("/^GuideSystem/", $fileName)) {
		$fileName = preg_replace("/GuideSystem/", "", $fileName);
		$file = GUIDE_HTDOCS_DIR . '/../shared/classes/' . $fileName . '.php';
		if (file_exists($file)) {
			require_once($file);
			return;
		}
	}
	if (preg_match("/^UnitSystem/", $fileName)) {
		$fileName = preg_replace("/UnitSystem/", "", $fileName);
		$file = UNIT_SYSTEM_HTDOCS_DIR . '/../shared/classes/' . $fileName . '.php';
		if (file_exists($file)) {
			require_once($file);
			return;
		}
	}
	throw new \GuideSystem\Exception\LogException("Could not find class $className");
}
spl_autoload_register('my_autoloader');
Logger::configure(GUIDE_HTDOCS_DIR . '../shared/configuration/log4php.xml');

function getUnitPartDir($unitDirectory, $partName) {
	$path = UNIT_SYSTEM_HTDOCS_DIR . "/../units/" . $unitDirectory . "/" . $partName;
	$canonisizedPath = realpath($path);
	if ($canonisizedPath === false)
		throw new \GuideSystem\Exception\LogException("Path does not exists ($path)");
	return $canonisizedPath . "/";
}

function redirect($location) {
	header('Location: ' . $location);
	exit();
}
