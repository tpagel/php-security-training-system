<?php
$smarty->registerPlugin("function", "getStatusCssClass", "getStatusCssClass");

function getStatusCssClass($object)
{
    if ($object->isActive()) {
        return "unitActive";
    }
    if ($object->isComplete()) {
        return "unitComplete";
    }
    return "unitUndone";
}
