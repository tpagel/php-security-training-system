#!/bin/bash
# Documentation: http://wiki.ubuntuusers.de/LiveCD_manuell_remastern?highlight=live

export WORK=/media/DATA2/livecd
sudo rm ${WORK}/ubuntu-livecd/casper/filesystem.squashfs
cd ${WORK}/new
sudo mksquashfs . ${WORK}/ubuntu-livecd/casper/filesystem.squashfs
cd ${WORK}
cd ${WORK}/ubuntu-livecd
sudo find . -type f -print0 |xargs -0 md5sum |sudo tee md5sum.txt 
cd $WORK
genisoimage     -o ${WORK}/php-security-trainingssystem.iso     -b isolinux/isolinux.bin     -c isolinux/boot.cat     -no-emul-boot     -boot-load-size 4     -boot-info-table     -r     -V "PHP-Sicherheits-Trainingsystem"     -cache-inodes      -J     -l     ubuntu-livecd 
