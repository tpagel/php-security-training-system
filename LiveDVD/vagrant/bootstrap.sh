#!/usr/bin/env bash

sudo apt-get update
sudo dpkg --force-depends -i /vagrant/*.deb 
sudo apt-get install -f -y
/etc/init.d/apache2 restart

echo "-----------------------------------------------------------------------"
echo "Please visit http://guidesystem.local in firefox to start the training"
echo "-----------------------------------------------------------------------"
