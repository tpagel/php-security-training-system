# $1 is the debian directory, $2 is the output directory

chown -R root:root $1

# finally build the package
dpkg-deb --build $1 $2
