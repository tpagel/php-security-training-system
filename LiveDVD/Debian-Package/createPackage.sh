#!/bin/bash
# See also http://programmers.stackexchange.com/questions/195633/good-approaches-for-packaging-php-web-applications-for-debian

rm *.deb
WWW_PATH=debian/var/www
DIR=$(pwd)
if [ -e $WWW_PATH/php-security-training-system/ ]; then
	sudo rm -Rf $WWW_PATH/php-security-training-system/
fi
mkdir -p $WWW_PATH > /dev/null
git clone git@bitbucket.org:tpagel/php-security-training-system.git $WWW_PATH/php-security-training-system
find . -maxdepth 2 -type d -name ".git" -exec rm -R '{}' \;
cd $DIR
SCRIPT_PATH=$WWW_PATH/php-security-training-system/scripts/
cd $SCRIPT_PATH
chmod 700 setup.bash
./setup.bash
cd $DIR

# finish through fakeroot so we can adjust ownerships without needing to be root    
fakeroot ./finishPackage.sh debian .

